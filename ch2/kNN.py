import numpy as np
import operator


def create_data_set():
    group = np.array([[1.0, 1.1], [1.0, 1.0], [0, 0], [0, 0.1]])
    labels = ['A', 'A', 'B', 'B']
    return group, labels


def classify0(in_x, data_set, labels, k):
    data_set_size = data_set.shape[0]
    diff_mat = np.tile(in_x, (data_set_size, 1)) - data_set
    sq_diff_mat = diff_mat ** 2
    sq_distances = sq_diff_mat.sum(axis=1)
    distances = sq_distances ** 0.5
    sorted_dist_indices = distances.argsort()
    class_count = {}

    for i in range(k):
        vote_i_label = labels[sorted_dist_indices[i]]
        class_count[vote_i_label] = class_count.get(vote_i_label, 0) + 1

    sorted_class_count = sorted(list(class_count.items()), key=operator.itemgetter(1), reverse=True)

    return sorted_class_count[0][0]


def file2matrix(file_name):
    data_l = []         # Data Set List
    labels_l = []       # Labels List

    # Read data from file.
    with open(file_name) as file:
        for line in file.readlines():
            line_l = line.strip().split('\t')
            data_l.append(line_l[:-1])
            labels_l.append(line_l[-1])

    # Return Data(numpy.ndarray) and Labels(numpy.ndarray)
    return np.array(data_l).astype(float), np.array(labels_l).astype(int)


def auto_norm(data_set):
    min_values = data_set.min(0)
    max_values = data_set.max(0)
    ranges = max_values - min_values
    m = data_set.shape[0]
    norm_data_set = data_set - np.tile(min_values, (m, 1))
    norm_data_set = norm_data_set / np.tile(ranges, (m, 1))
    return norm_data_set, ranges, min_values


def dating_class_test():
    ho_ratio = 0.10
    dating_data, dating_labels = file2matrix('datingTestSet2.txt')
    norm_mat, ranges, min_vals = auto_norm(dating_data)
    m = norm_mat.shape[0]
    num_test_vecs = int(m * ho_ratio)
    error_count = 0.0

    for i in range(num_test_vecs):
        classifier_result = classify0(norm_mat[i, :], norm_mat[num_test_vecs:m, :],
                                      dating_labels[num_test_vecs:m], 7)
        if classifier_result != dating_labels[i]:
            error_count += 1.0

    print("The total error rate is: {error}".format(error=(error_count / float(num_test_vecs))))


def classify_person():
    result_l = ['not at all', 'in small doses', 'in large doses']
    game_time = float(input("Percentage of time spent playing video games: "))
    ffmiles = float(input("Frequent Flier Miles earned per year: "))
    ice_cream = float(input("Liters of Ice Cream consumed per year: "))
    dating_data, dating_labels = file2matrix("datingTestSet2.txt")
    norm_mat, ranges, min_vals = auto_norm(dating_data)
    input_arr = np.array([ffmiles, game_time, ice_cream])
    classifier_result = classify0((input_arr - min_vals)/ranges, norm_mat, dating_labels, 7)
    print("You'll probably like this person: ", result_l[classifier_result - 1])
